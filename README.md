Confident
=========

Confident is a Rails gem-plugin which handles user 
confidentiality agreements.  It prevents access to resources
without a signed confidentiality agreement and provides an interface
to sign said agreement.

### Configuration

To configure Confident create an initializer in initializers/confident.rb. 

    Confident.configure{ :conf_model => Signature, 
                         :conf_file => "../../agreement.txt",
                         :conf_host_hook => {|env| env['aker.check']}
 
The class passed as conf_model must be respond to signed? and sign methods which take
a Aker::User as arguments.  The conf_file option is a path to the file
containing the desired text for the confidentiality agreement. The conf_host_hook
option is an object which responds to call and receives the rack env. The hook 
determines whether confident will be active or not on a request. If the hook returns
true, the request will be passed through to the app skipping Confident.


### Running Tests

The tests which do not require a full Rails application are in the top level
of the spec/ directory.  Within integration are specs requiring a Rails application.

In order to set up the testbed application and run Rails integration tests:

    rake -f init_testbed.rakefile 
    cd testbed
    rspec ../spec/integration/integration_spec.rb
