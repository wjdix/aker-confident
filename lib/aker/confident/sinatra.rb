require 'sinatra/base'
require 'haml'
module Aker
  module Confident
    class Sinatra < Sinatra::Base
      set :views, File.dirname(__FILE__) + '/../../../assets/templates'
      get // do
        @original_target = request.url
        @conf_agreement = Aker::Confident.configuration.agreement
        @root_url = request.scheme + "://" + request.host_with_port + request.script_name
        haml :agreement
      end
    end
  end
end
