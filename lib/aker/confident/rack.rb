require 'rack/request'
module Aker
  module Confident
    class Rack
      def initialize(app)
        @app = app
      end

      def call(env)
        if Aker::Confident.pass_through?(env)
          @app.call(env)
        else
          if env['REQUEST_METHOD'] == 'POST'
            env['aker.check'].user.sign
            req = ::Rack::Request.new(env)
            [303, {"Location" => "#{req[:original]}"}, ""]
          else
            Aker::Confident::Sinatra.call(env)
          end
        end
      end
    end
  end
end
