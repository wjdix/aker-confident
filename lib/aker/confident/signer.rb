module Aker
  module Confident
    module Signer
      
      def signed?(user)
        validate_argument!(user)
        configuration.model.signed?(user)
      end
      
      def sign(user)
        validate_argument!(user)
        configuration.model.sign(user)
      end
      
      
      def validate_argument!(arg)
        unless arg.is_a? Aker::User
          raise ArgumentError.new("Requires an instance of Aker::User as argument")
        end
      end
      private :validate_argument!
    end
  end
end
