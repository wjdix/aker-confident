module Aker
  module Confident
    class AkerSlice < Aker::Configuration::Slice
      def initialize
        super do
          after_authentication_middleware do |builder|
            builder.use Confident::Rack
          end
        end
      end
    end
  end
end
