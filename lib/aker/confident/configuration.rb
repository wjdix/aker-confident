module Aker
  module Confident
    class ConfigurationError < RuntimeError
    end

    class Configuration
      attr_accessor :agreement, :root_url, :host_hook
      attr_reader :pass_through_hooks
      def initialize(options_hash={})
        @pass_through_hooks = []
        conf_model(options_hash[:conf_model])
        conf_file(options_hash[:conf_file])
        set_url options_hash[:root_url]
        if options_hash[:conf_host_hook]
          conf_host_hook options_hash[:conf_host_hook]
        end
      end

      def define &block
        instance_eval &block
        self
      end

      def conf_model(model_name)
        if model_name == :default
          @model = Confident::DefaultSignature
        else
          @model = model_name
        end
      end

      def set_url(url)
        @root_url = url
      end

      def conf_file(file_name)
        @agreement_file = file_name
      end

      def conf_host_hook(callable)
        @pass_through_hooks << callable
      end

      def verify!
        validate_agreement_file!
        load_agreement_file
        validate_hook!
        validate_model!
      end

      def agreement
        @agreement ||= load_agreement_file
      end

      def model
        if @model.is_a? String
          @model.constantize
        else
          @model
        end
      end

      private

      def validate_agreement_file!
        raise Aker::Confident::ConfigurationError unless @agreement_file.present?
      end

      def load_agreement_file
        begin
          @agreement = File.read(@agreement_file)
        rescue Errno::ENOENT => e
          raise Aker::Confident::ConfigurationError.new "The specified file does not exist"
        end
      end

      def valid_option_keys?(options_hash)
        options_hash.keys.include?(:conf_model) &&
          options_hash.keys.include?(:conf_file)
      end
      private :valid_option_keys?

      def valid_conf_model_value?(options_hash)
        if options_hash[:conf_model].is_a? Symbol
          options_hash[:conf_model] == :default
        elsif options_hash[:conf_model].is_a? String
          !options_hash[:conf_model].empty?  
        else
          !options_hash[:conf_model].nil?
        end
      end
      private :valid_conf_model_value?

      def valid_conf_file_value?(options_hash)
        if options_hash[:conf_file].is_a? String
          !options_hash[:conf_file].empty?
        else
          !options_hash[:conf_file].nil?
        end
      end
      private :valid_conf_file_value?

      def validate_model!
        unless model.respond_to?(:signed?)
          raise Aker::Confident::ConfigurationError.new "Confidentiality must respond to signed? method."
        end
        unless model.respond_to?(:sign)
          raise Aker::Confident::ConfigurationError.new "Confidentiality must respond to sign method."
        end
      end

      def validate_hook!
        unless pass_through_hooks.empty? 
          unless pass_through_hooks.all?{ |hook| hook.respond_to?(:call)}
            raise Aker::Confident::ConfigurationError.new "Host hook must respond to call"
          end
        end
      end

      def validate_options(options_hash)
        unless valid_option_keys?(options_hash)
          raise Aker::Confident::ConfigurationError.new "Valid Configurations require a conf_model and a conf_file!" 
        end
        unless valid_conf_model_value?(options_hash) && valid_conf_file_value?(options_hash)
          raise Aker::Confident::ConfigurationError.new "Valid Configuration conf_model and conf_file cannot be nil or blank"
        end
      end
      private :validate_options
    end
  end
end
