module Aker
  module Confident
    module AkerUserExt
      def sign
        @signed = Confident.sign(self)
      end
      def signed?
        @signed ||= Confident.signed?(self)
      end
    end
  end
end
