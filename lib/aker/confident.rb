require 'active_support'
require 'aker'
module Aker
  module Confident
    autoload :Configuration,  'aker/confident/configuration'
    autoload :Signer, 'aker/confident/signer'
    autoload :Sinatra, 'aker/confident/sinatra'
    autoload :Rack, 'aker/confident/rack'
    autoload :DefaultConfiguration, 'aker/confident/configuration'
    autoload :AkerUserExt, 'aker/confident/aker_user_ext'
    autoload :AkerSlice, 'aker/confident/aker_slice'

    class << self
      include Signer
      attr_accessor :configuration

      def configuration
        @configuration
      end

      def configure(hash_options={}, &config_block)
        if config_block
          @configuration = Confident::Configuration.new(hash_options).define &config_block
        else
          @configuration = Confident::Configuration.new(hash_options)
        end
        @configuration.verify!
      end

      def conf_model
        @configuration.model
      end
      def root_url
        @configuration.root_url
      end
      def host_hook
        @configuration.host_hook
      end

      def pass_through?(env)
        hooks.any?{|hook| hook.call(env) }
      end

      # if true Confident should intercept requests
      def default_pass_through_hooks
        [
          lambda {|env| !env['aker.interactive'] },
          lambda {|env| (env['aker.check'] && env['aker.check'].user == nil) },
          lambda {|env| env['PATH_INFO'] == '/logout' },
          lambda {|env| env['REQUEST_METHOD'] == 'POST' && env['PATH_INFO'] != '/sign_agreement' },
          lambda {|env| env['aker.check'].user.signed? }
        ]
      end

      def hooks
        default_pass_through_hooks + configuration.pass_through_hooks
      end
    end
    Aker::Configuration.add_default_slice Confident::AkerSlice.new
    Aker::User.send(:include, Confident::AkerUserExt)
  end
end
