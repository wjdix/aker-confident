require 'spec_helper'

describe Aker::Confident::Sinatra do
  include Rack::Test::Methods
  def app
    configure_conf
    @app = Aker::Confident::Sinatra
  end
  
  it "should respond to /sign_agreement" do
    get '/sign_agreement'
    last_response.should be_ok
  end
  
  it "should render the agreement from the configuration" do
    get '/sign_agreement'
    last_response.body.should include "This is an agreement"
  end

  describe "capybara tests" do
    
  end
  
end
