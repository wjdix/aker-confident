require 'spec_helper'

describe Aker::Confident do
  describe "#configure" do
    let(:dummy_conf){ stub.as_null_object }
    it "builds a new config object" do
      Aker::Confident::Configuration.should_receive(:new).and_return dummy_conf
      Aker::Confident.configure
    end
    it "uses a block to define the configuration" do
      Signature = Class.new
      def Signature.signed? ; end
      def Signature.sign ; end
      Aker::Confident.configure do
        conf_file  __FILE__
        conf_model Signature
      end
      Aker::Confident.configuration.model.should == Signature
      Aker::Confident.configuration.agreement.should == File.read(__FILE__)
    end
    it "verifys the configuration" do
      Aker::Confident::Configuration.stub(:new).and_return dummy_conf
      dummy_conf.stub(:define).and_return dummy_conf
      dummy_conf.should_receive(:verify!)
      Aker::Confident.configure do ; end 
    end
  end
end
