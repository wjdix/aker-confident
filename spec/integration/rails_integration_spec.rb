#this file will be run within the context of the testbed project
require File.join(File.dirname(__FILE__), '../..', 'testbed', 'spec', 'spec_helper')

describe "Rails integration" do
  
  context "when conf agreement is signed" do
    before(:each) do
      Signature.create!(:username => 'wakibbe')
    end
    it "should not redirect to conf_agreement", :type => :integration do
      visit '/labs'
      login
      current_path.should =~ /labs/
      visit '/logout'
    end
  end
  
  context "when conf agreement is unsigned" do
    before(:each) do
      Signature.destroy_all
    end
    it "should display the conf_agreement", :type => :integration do
      visit '/logout'
      visit '/login'
      visit '/labs'
      login
      page.should have_content("Don't share this, dirtbag")
    end
    
    it "should let me sign the conf agreement and redirect to original target", :type => :integration do
      visit '/logout'
      visit '/labs'
      login
      click_on 'I agree and accept the terms'
      current_path.should =~ /labs/
      page.should have_content("Lab")
    end
    
    it "should show me the conf agreement", :type => :integration do
      visit '/logout'
      visit '/labs'
      login
      page.should have_content("Don't share this, dirtbag")
      visit '/logout'
    end
  end
  

end


def login
  fill_in "username", :with => "wakibbe"
  fill_in "password", :with => "password"
  click_on "Log in"
end
