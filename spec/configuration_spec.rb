require 'spec_helper'
require 'active_record'

describe Aker::Confident::Configuration do


  def verify(options)
    lambda{ Aker::Confident::Configuration.new(options).verify! }
  end
  before(:each) do
    @valid_options = {
      :conf_model => Agreement,
      :conf_file => __FILE__,
      :conf_host_hook => Proc.new{ |env| 4}
    }
  end 

  describe "verify!" do
    it "should check that agreement responds to signed?" do
      @valid_options[:conf_model] = NoSignedAgreement
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError
    end
    it "should check that agreement responds to sign" do
      @valid_options[:conf_model] = NoSignAgreement
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError
    end

    it "checks that hook responds to call" do
      @valid_options[:conf_host_hook] = 4 
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError 
    end

    it "is valid without hooks" do
      @valid_options.delete :conf_host_hook
      verify(@valid_options).should_not raise_error Aker::Confident::ConfigurationError 
    end

    it "raises an error if no conf model is nil or blank" do
      @valid_options[:conf_model] = nil
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError
      @valid_options[:conf_model] = ""
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError
    end

    it "raises an error if file at conf_file path is not present" do
      @valid_options[:conf_file] = "junkfilethatcouldntpossiblyexistifitdiditwouldbeahugecoincidence"
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError
    end

    it "raises an error if no conf_file key in options" do
      verify({:conf_model => Agreement}).should raise_error Aker::Confident::ConfigurationError
    end

    it "raises an error if conf_file is nil or blank" do
      @valid_options[:conf_file] = nil
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError
      @valid_options[:conf_file] = ""
      verify(@valid_options).should raise_error Aker::Confident::ConfigurationError
    end
  end

  it 'works with a block of config options' do
    stub_proc = Proc.new{ |env| 4 }
    conf = Aker::Confident::Configuration.new.define do 
      conf_model Agreement
      conf_file __FILE__
      conf_host_hook stub_proc
    end
    conf.model.should == Agreement
    conf.agreement.should == File.read(__FILE__)
    conf.pass_through_hooks.should include stub_proc
  end

  it "accepts a hash of config options to set configuration" do
    config = Aker::Confident::Configuration.new(@valid_options)
    config.should_not be_nil
  end

  it "reads conf agreement from supplied file path" do
    config = Aker::Confident::Configuration.new(@valid_options)
    config.agreement.should == File.read(__FILE__)
  end

  it "sets conf model based on options" do
    @valid_options[:conf_model] = Agreement
    config = Aker::Confident::Configuration.new(@valid_options)
    config.model.should == Agreement
  end

  it "returns constantized model if given string" do
    @valid_options[:conf_model] = "Agreement"
    config = Aker::Confident::Configuration.new(@valid_options)
    config.model.should == Agreement
  end


  it "takes a root_url configuration option" do
    @valid_options[:root_url] = "http://test.host"
    config = Aker::Confident::Configuration.new(@valid_options)
    config.root_url.should == "http://test.host"
  end
  
end
