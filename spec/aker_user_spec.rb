require 'spec_helper'

module Aker
  class User
    #add this for convenience in testing
    attr_accessor :signed
  end
end

describe Aker::User do
  before(:all) do
    Aker::User.send(:include, Aker::Confident::AkerUserExt)
  end
  before(:each) do
    @user = Aker::User.new(1)
  end
  describe "#signed?" do
    it "is true if confident.signed? is true" do
      Aker::Confident.stub(:signed?).and_return(true)
      @user.signed?.should be_true
    end
    it "sets the users signed attribute" do
      Aker::Confident.stub(:signed?).and_return(false)
      @user.signed.should be_false
    end
  end
  describe "#sign" do
    it "calls confidents sign method" do
      Aker::Confident.should_receive(:sign).with(@user).and_return(true)
      @user.sign
    end
    it "updates the users signed attribute" do
      Aker::Confident.should_receive(:sign).with(@user).and_return(true)
      @user.sign
      @user.signed.should == true
    end
  end
end
