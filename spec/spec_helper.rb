require 'lib/aker-confident'
require 'rack/test'
require 'aker/rails'


include Aker::Rails::Test::Helpers
def configure_conf
  file_path = File.join(File.dirname(__FILE__), "assets/test_agreement.txt")
  options = {
    :conf_model => Agreement,
    :conf_file => file_path,
  }
  Aker::Confident.configure(options)
end

class Agreement
  def self.signed?(user)
  end
  def self.sign(user)
  end
end

class NoSignAgreement
  def self.signed?(user)
  end
end

class NoSignedAgreement
  def self.sign(user)
  end
end
