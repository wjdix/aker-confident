require 'spec_helper'

describe Aker::Confident::Rack do 
  include Rack::Test::Methods
  
  class MockApp
    def call(env)
      [200, {}, 'Hello from Mock App!']
    end
  end
  
  def app
    configure_conf
    @app = Rack::Builder.new {
      use Aker::Confident::Rack
      run MockApp.new
    }
    @app.to_app
  end
  
  context "a non-interactive request" do
    let(:env){ {"aker.interactive" => false} }
    it "should ignore all non-interactive requests" do
      get '/foo', {}, env
      last_response.body.should include "Hello from Mock App!"
    end
  end

  context "there is a pass_through_hook defined" do
    let(:env){ {"aker.check" => stub(:user => stub(:signed? => false)), "aker.interactive" => true} }
    let(:mock_app){ mock(:call => [200, {}, 'ok']) }
    it 'calls the host hook with the rack env' do
      host_hook = stub
      Aker::Confident.stub(:configuration).
        and_return (stub(:pass_through_hooks => [host_hook]))
      host_hook.should_receive(:call)
      get '/foo', {}, env
    end
    context 'the hook is true' do
      let(:host_hook){ lambda{ |env| true } }
      before do
        MockApp.stub(:new).
          and_return mock_app
        Aker::Confident.stub(:configuration).
         and_return stub(:pass_through_hooks => [host_hook])
      end
      it 'skips confident' do 
        mock_app.should_receive(:call)
        get '/foo', {}, env
      end
    end
    context 'the hook is false' do
      let(:host_hook){ lambda{ |env| false } }
      before do
        MockApp.stub(:new).
          and_return mock_app
        Aker::Confident.stub(:configuration).
          and_return stub(:pass_through_hooks => [host_hook])
      end
      it 'calls sinatra' do
        Aker::Confident::Sinatra.should_receive(:call).and_return [200, {}, 'ok']
        get '/foo', {}, env
      end
    end
  end

  context "an interactive request" do
    let(:mock_user){ stub(:signed? => false) }
    let(:env){
      {"aker.check" => stub(:user => mock_user),
       "aker.interactive" => true}
    }
    context "post /sign_agreement" do
      it "should call aker user sign method" do
        mock_user.should_receive(:sign).and_return true
        post '/sign_agreement', {:original => "/foo"}, env
      end
      it "should redirect to url in params" do
        mock_user.stub(:sign).and_return true
        post '/sign_agreement', {:original => "/foo"}, env 
        last_response.should be_redirect
        last_response.headers['Location'].should == "/foo" 
      end
    end

    context "post elsewhere" do
      before do
        @mock_app = mock(:call => [200, {}, 'ok'])
        MockApp.stub(:new).
          and_return @mock_app
      end
      it "should call the app" do
        @mock_app.should_receive(:call)
        post '/junk', {}, env
      end
    end
    context "get logout" do
      before do
        @mock_app = mock(:call => [200, {}, 'ok'])
        MockApp.stub(:new).
          and_return @mock_app
      end
      it "calls the app" do
        @mock_app.should_receive(:call)
        get '/logout', {}, env
      end
    end
  
  
    context "when agreement is not signed" do
      before(:each) do
        mock_user.stub(:signed?).and_return false
      end
      it "should call the confident sinatra" do
        Aker::Confident::Sinatra.should_receive(:call).
          and_return [200, {}, 'Foo']
        get '/foo', {}, env 
      end
    end
  
    context "when agreement is signed" do
      before(:each) do
        mock_user.stub(:signed?).and_return true
      end
      it "should call the app" do
        get '/foo', {}, env
        last_response.should be_ok
        last_response.body.should include "Hello from Mock App!"
      end
    end
  end
end
