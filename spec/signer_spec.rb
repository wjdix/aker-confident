require 'spec_helper'
describe Aker::Confident::Signer do
  before(:each) do
    @valid_options = {:conf_model => Agreement,
                      :conf_file => __FILE__}
    Aker::Confident.configure(@valid_options)
    @valid_user = Aker::User.new("foo")
  end
  it "should delegate signed? to conf_model" do
    Aker::Confident.configuration.model.should_receive(:signed?).with(@valid_user)
    Aker::Confident.signed?(@valid_user)
  end
  it "should delegate sign to conf_model" do 
    Aker::Confident.configuration.model.should_receive(:sign).with(@valid_user)
    Aker::Confident.sign(@valid_user)
  end
  it "should require a bcsec user as argument to signed?" do
    lambda {Aker::Confident.signed?(4)}.should raise_error ArgumentError
  end
  it "should require a bcsec user as argument to sign" do
    lambda {Aker::Confident.sign(4)}.should raise_error ArgumentError
  end
end
