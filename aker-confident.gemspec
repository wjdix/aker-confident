# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "aker/confident/version"

Gem::Specification.new do |s|
  s.name        = "aker-confident"
  s.version     = Aker::Confident::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["William Dix", "Peter Nyberg"]
  s.email       = ["wjdix@northwestern.edu"]
  s.homepage    = ""
  s.summary     = %q{Confidentiality gem for Aker }
  s.description = %q{ialdskj}

  s.rubyforge_project = "aker-confident"
  
  s.add_dependency('aker')
  s.add_dependency('aker-rails')
  s.add_dependency('sinatra')
  s.add_dependency('haml')
  s.add_dependency('schema_qualified_tables')
  
  s.add_development_dependency('rails', "3.0.11")
  s.add_development_dependency('rspec')
  s.add_development_dependency('rspec-rails')
  s.add_development_dependency('capybara', '0.3.7')
  s.add_development_dependency('rack-test')
  s.add_development_dependency('sqlite3')

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end
