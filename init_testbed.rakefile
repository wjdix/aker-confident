desc "Set up a rails app for testing in the spec dir"
task :default => [:"testbed:generate", :"testbed:generate_resource", :"testbed:generate_sig_model", :"testbed:migrate", 
                  :"testbed:config_confident", :"testbed:conf_agreement", :"testbed:bcsec"]

namespace "testbed" do
  # "testbed" is also hardcoded in the spec/spec_helper.rb features/support/env.rb and gitignore file. Change it there too...
  
  desc "Generate rails, rspec, cucumber"
  task :generate do
    chdir("testbed") do
      sh "bundle install"
      sh "bundle exec rails new . --skip-gemfile"
      sh "bundle exec rails generate rspec:install"
      sh "rm -rf features"
      spec_helper = <<-HELPER
      ENV["RAILS_ENV"] ||= 'test'
      require File.expand_path(File.join(File.dirname(__FILE__),'..','config','environment'))
      require 'spec/autorun'
      require 'spec/rails'
      require 'capybara/dsl'
      require 'capybara/rails'
      Dir[File.expand_path(File.join(File.dirname(__FILE__),'support','**','*.rb'))].each {|f| require f}

      Spec::Runner.configure do |config|
        # If you're not using ActiveRecord you should remove these
        # lines, delete config/database.yml and disable :active_record
        # in your config/boot.rb
        config.use_transactional_fixtures = false
        config.use_instantiated_fixtures  = false
        config.fixture_path = RAILS_ROOT + '/spec/fixtures/'
        config.include(Capybara, :type => :integration)
        Capybara.current_driver = :selenium
      end
      HELPER
    end
  end
 
  desc "create sample resource"
  task :generate_resource do
    chdir("testbed") do
      sh "bundle exec rails generate scaffold lab name:string result:integer"
    end
  end
  
  desc "create model for conf signature"
  task :generate_sig_model do
    chdir("testbed") do
      sh "bundle exec rails generate model Signature username:string created_at:date"
      sig_model = <<-MODEL
      class Signature < ActiveRecord::Base
        def self.signed?(user)
          find_by_username(user.username)
        end
        def self.sign(user)
          create(:username => user.username)
        end
      end
      MODEL
      File.open('app/models/signature.rb', 'w') do |f|
        f.write sig_model
      end
    end
  end

  desc "Generate, migrate testbed"
  task :migrate do
    chdir("testbed") do
      sh "bundle exec rake db:migrate db:test:prepare"
    end
  end
  
  desc "add initializer file for confident"
  task :config_confident do
    chdir("testbed") do
      conf_config = "Aker::Confident.configure({:conf_model => Signature, :conf_file => File.join(File.dirname(__FILE__), \"..\", \"..\", \"conf_agreement.txt\")})\n"
      File.open("config/initializers/confident.rb", 'w'){ |f| f.write conf_config }
    end
  end
  
  desc "add initializer file for bcsec"
  task :bcsec do
    chdir("testbed") do
      bcsec_conf = <<-CONF
      Aker.configure {
        ui_mode :form
        portal :test
        authority Aker::Authorities::Static.from_file \"\#{RAILS_ROOT}/../spec/assets/test-users.yml\"
      }
      CONF
      File.open("config/initializers/aker.rb", 'w'){ |f| f.write bcsec_conf }
      con_file = File.read "app/controllers/application_controller.rb"
      File.open("app/controllers/application_controller.rb", 'w') do |f|
        f.write con_file.sub!("class ApplicationController < ActionController::Base", "class ApplicationController < ActionController::Base\n  include Aker::Rails::SecuredController")
      end
    end
  end
  
  desc "add conf_agreement file"
  task :conf_agreement do
    chdir("testbed") do
      conf_agreement = "Don't share this, dirtbag!"
      File.open("conf_agreement.txt", 'w'){|f| f.write conf_agreement}
    end
  end

  desc "Remove testbed app"
  task :remove do
    puts "Removing the test_app in the spec folder"
    chdir("testbed") do
      sh 'rm -rf Gemfile.lock README Rakefile app config db doc features lib log public script spec surveys test tmp vendor conf_agreement.txt'
    end
  end
end
